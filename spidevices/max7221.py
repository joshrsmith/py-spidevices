"""8 digit LED Display Driver

My more detailed description.

"""
from spidevice import spidevice

class MAX7221(spidevice):
   
   """8 digit LED Display Driver
   
   Conventions for this class:
      - Display digit positions range from D0-D7
      
   """
   
   NOOP_ADDR=0x0
   DIGIT_ADDR=range(1,8)
   DECODE_MODE_ADDR=0x9
   INTENSITY_ADDR=0xa
   SCAN_LIMIT_ADDR=0xb
   SHUTDOWN_ADDR=0xc
   DISPLAY_TEST_ADDR=0xf
   
   def __init__(self, spidev):
      super(MAX7221, self).__init__(spidev)
   def testmode(self, on=True):
      """Enable/disable driver testmode.
      
      on -- True = Enable testmode; False = Disable testmode
      
      Testmode will enable all display drivers for display testing purposes. Display will be at maximum intensity. Testmode bypasses the state of shutdown.
      
      """
      
      if on:
         self.__write_display(self.DISPLAY_TEST_ADDR, 1)
      else:
         self.__write_display(self.DISPLAY_TEST_ADDR, 0)
   def decode_mode(self, digits, codeb=True):
      """Set the display value decoding mode.
      
      digits -- list of display digit positions to update decoding mode
      codeb -- True = Enable codeb decoding; False = Enable normal decoding

      MAX7221 supports two modes of decoding. Normal operation, and Code B. Under normal operation, each bit of 1 byte written to a digit register is used to turn on/off a segment of the display. In Code B operation, the decimal value of number written to the register is displayed. For example, b'11 is displayed as "3" on the display.
      
      In general, Code B greatly simplifies displaying numerical values.
      
      When using Code B decoding, writing a value of 15 (0xF) to a digit position will result in a "blank" digit state.      
      
      """
      
      enable = 0
      for i in range(len(digits)):
         if codeb:
            enable |= (1 << digits[i])
      self.__write_display(self.DECODE_MODE_ADDR, enable)
   def scan_mode(self, display_digits_up_to=3):
      """Set the number of digits that should be periodically updated (scanned) by the driver.
      
      display_digits_up_to -- Specify the largest digit position that should be updated/displayed by the driver. For example, if driving a 4 digit display, specify display_digits_up_to=3.
      
      If a digit is not scanned, it will appear blank (off). MAX7221 supports up to 8 digit positions. If fewer than 8 digit positions are desired, then the scan mode can reduce the number of digits to update. The fewer the digits that are updated, the more frequently digits can be refreshed by the driver.
      
      If it is desired to sometimes display a blank digit on the display, one should use the digit controls instead of scan mode, since scan mode will also affect the apparent brightness/intensity of the display.
      
      """
      self.__write_display(self.SCAN_LIMIT_ADDR, display_digits_up_to)
      
   def set_digit(self, digit, value):
      """Write value to digit position digit. The current state of decoding mode for the digit will impact what is actually displayed."""
      self.__write_display(self.DIGIT_ADDR[digit], value)
      
   def shutdown(self, shutdown=True):
      """Put the display in shutdown, or bring display out of shutdown.
      
      shutdown -- True = Put device into shutdown state (all display digits disabled)
                  False = Bring device out of shutdown (all display digits enabled)
      
      """
      if not shutdown:
         self.__write_display(self.SHUTDOWN_ADDR, 1)
      else:
         self.__write_display(self.SHUTDOWN_ADDR, 0)
         
   def intensity(self, intensity):
      """Set the intensity for all digits of the display.
      
      intensity -- % intensity, (0.0) most dim to (1.0) full brightness
      
      Display intensity is relative to the number of digits being scanned and the hardware configuration.
      
      """
      value = int(intensity * 16)
      if value > 15:
         value = 15
      elif value < 0:
         value = 0
      self.__write_display(self.INTENSITY_ADDR, value)
      
   def __write_display(self, addr, data):
      """Write data to addr
      
      addr -- 1-byte SPI address of registers of MAX7221
      data -- 8-bit SPI data to write to register
      
      """
      values_in = [addr, data]
      self.dev.xfer(values_in)
      