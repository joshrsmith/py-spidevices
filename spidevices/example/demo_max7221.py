from time import sleep
import spidev
from spidevices import max7221

# create/connect to device at /dev/spidev0.1
dev = spidev.SpiDev(0,1)
dev.mode = 0
dev.max_speed_hz = 500000
dev.cshigh = False
dev.lsbfirst = False
dev.threewire = False
dev.loop = False # Loopback
dev.bits_per_word = 8
max7221 = max7221.MAX7221(dev)
max7221.testmode(on=True)
sleep(5)
max7221.testmode(on=False)
max7221.decode_mode(range(4), codeb=True)
max7221.scan_mode(display_digits_up_to=3)
max7221.set_digit(0, 5)
max7221.set_digit(1, 5)
max7221.set_digit(2, 5)
max7221.set_digit(3, 5)
max7221.shutdown(shutdown=False)
sleep(5)
max7221.intensity(.1)
sleep(2)
max7221.intensity(.5)
sleep(2)
max7221.intensity(1.0)
sleep(2)
max7221.shutdown()
dev.close()