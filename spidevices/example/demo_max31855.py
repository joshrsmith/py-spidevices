import spidev
from spidevices import max31855
from time import sleep

dev = spidev.SpiDev(0,0)
dev.mode = 0
dev.max_speed_hz = 500000
dev.cshigh = False
dev.lsbfirst = False
dev.threewire = False
dev.loop = False # Loopback
dev.bits_per_word = 8
max31855 = max31855.MAX31855(dev)
for i in range(30):
   measurement = max31855.read_temperature()
   print "Internal Temp = %3.1f, Thermocouple = %3.1f" % (measurement.internal_temp, measurement.thermocouple_temp)
   sleep(1)
dev.close()