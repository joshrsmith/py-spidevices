"""Control for various SPI devices/ICs using spidev interface

Implements interfaces to various SPI-compatible devices/integrated circuits.  One python module per device supported is implemented. Each module wraps all of SPI related controls of an SPI device such as addressing, bit-packing and unpacking.  For example, a module for a temperature chip would need to implement functionality to read temperature values from the device.  Functionality is as "dumb" as possible; a simple implementation of the ICD.  Unit conversions, etc. should be handled elsewhere.  Using this package should remove the need for a user to reference the datasheet of the device.

All modules interact with SPI devices through use of spidev-compatible interface. The spidev package is not a requirement, but modules included were tested using this interface. spidev can be found at https://bitbucket.org/joshrsmith/py-spidev

Supported Devices:
max7221 - 8 digit LED Display Driver
max31855 - Cold junction compensated thermocouple-to-digital converter

"""