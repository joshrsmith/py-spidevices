"""Base functionality required by all SPI compatible devices"""

class spidevice(object):
   def __init__(self, spidev):
      """Accept spidev, a spidev compatible device, and construct the spidevice"""
      self.dev = spidev