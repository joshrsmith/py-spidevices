"""Cold junction compensated thermocouple-to-digital converter"""

from spidevice import spidevice

def c(temp_c):
   pass

def c_to_f(temp_c):
   return (9/5)*temp_c+32.0

class TemperatureMeasurement(object):
   def __init__(self, temp_conv_function=c_to_f):
      self.convert = temp_conv_function

class Error(Exception):
   pass
      
class DataIntegrityError(Error):
   def __init__(self, value):
      self.value = value
   def __str__(self):
      return repr(self.value)

class CircuitError(Error):
   def __init__(self, scv, scg, oc):
      self.scv = scv
      self.scg = scg
      self.oc = oc
   def __str__(self):
      return "SCV = %d, SCG = %d, OC = %d" % (scf, scg, oc)
      
class MAX31855(spidevice):
   XFER_WIDTH = 4 # bytes
   def __init__(self, spidev):
      super(MAX31855, self).__init__(spidev)
   def read_temperature(self):
      values_in = [0x0, 0x0, 0x0, 0x0]
      values_out = self.dev.xfer(values_in)
      data32 = 0
      for i in range(self.XFER_WIDTH):
         data32 |= (values_out[i] << (8*(self.XFER_WIDTH-(i+1))));
      
      thermocouple_temp = ((data32 & 0xfffc0000) >> 18);
      if (thermocouple_temp & 0x2000):
         thermocouple_temp -= 2**13
      internal_temp = ((data32 >> 4) & 0xfff)
      if (internal_temp & 0x800):
         internal_temp -= 2**11
      reserved1 = ((data32 >> 17) & 0x1)
      reserved2 = ((data32 >> 3) & 0x1)
      top_fault = ((data32 >> 16) & 0x1)
      short_circuit_vcc = (data32 & 0x4)
      short_circuit_gnd = (data32 & 0x2)
      open_circuit = (data32 & 0x1)
	
      if (reserved1 or reserved2):
         raise DataIntegrityError("Reserved1 or Reserved2 were set")
      elif top_fault:
         raise CircuitError(short_circuit_vcc,
                            short_circuit_gnd,
                            open_circuit)
      
      measurement = TemperatureMeasurement()
      measurement.internal_temp = (internal_temp*0.0625)
      measurement.thermocouple_temp = (thermocouple_temp*0.25)
      return measurement
      