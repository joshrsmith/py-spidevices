#!/usr/bin/env python

from distutils.core import setup

setup(
   name = "spidevices",
   version = "1.0",
   description = "Control for various SPI devices/ICs through Linux spidev interface",
   author = "Josh Smith",
   author_email = "joshrsmith@gmail.com",
   maintainer = "Josh Smith",
   maintainer_email = "joshrsmith@gmail.com",
   license = "MIT",
   url = "https://bitbucket.org/joshrsmith/py-spidevices/",
   packages = ['spidevices'],
   platforms = ['linux'],
   long_description = "Control for various SPI devices/ICs through spidev interface. The spidev interface used in development and test of this package is found at https://bitbucket.org/joshrsmith/py-spidev.  Note that this package is not directly imported, and therefore not strictly necessary. However, spidevices assumes that the SPI device provided to spi device classes has methods that mimic those of py-spidev; primarily spidev.SpiDev.xfer.",
   )