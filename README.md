# py-spidevices #

(c) 2013 Joshua Smith. See LICENSE for details.

A pure-python package of interfaces to SPI devices / integrated circuits.

This package abstracts the details of a semiconductor datasheet to a python object where interaction occurs with the actual device through method calls. For example, when interfacing with a [MAX31855](http://www.maximintegrated.com/datasheet/index.mvp/ver/F/id/7273/t/), instead of needing to worry about the details of which bits are thermocouple temperature, what the scaling factor is, etc you can just `read_temperature()`.

This package does **not** directly require the use of [py-spidev](https://bitbucket.org/joshrsmith/py-spidev), but does require a "spidev compatible" interface. The most important of the interface requirements is `xfer()` which transfer bytes to/from SPI compatible device.

----------

## Example Use ##

See py-spidevices/spidevices/example/demo_*

From Linux command line:

`~/py-spidevices $ sudo python -m spidevices.example.demo_max7221`

----------

## References ##

- [Linux Kernel spidev documentation](https://www.kernel.org/doc/Documentation/spi/spidev)
- [py-spidev](https://bitbucket.org/joshrsmith/py-spidev)
